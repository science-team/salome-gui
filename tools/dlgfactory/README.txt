This package provides a simple tool to generates the bootstrap files
of a standard Qt dialog. Nothing original neither very smart, but just
help to initiate all this stuff the good way.

See the header of the script dlgfactory.py for details.

Some use cases (basic unit test) are given in the files qtester.cxx
and gtester.cxx. The build procedure (when you type make) create test
classes called QDialogTest and GDialogTest and used respectively in
qtester and gtester.

(gboulant - 26 oct. 2011)
