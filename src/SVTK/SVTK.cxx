// Copyright (C) 2007-2012  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  SALOME VTKViewer : build VTK viewer into Salome desktop
//  File   :
//  Author :

#include "SVTK.h"
#include "SVTK_Prs.h"
#include "SVTK_Actor.h"
#include "SALOME_Actor.h"
#include "SVTK_RectPicker.h"
#include "SVTK_DeviceActor.h"
#include "SVTK_CubeAxesActor2D.h"
#include "SVTK_Functor.h"
#include "SVTK_View.h"
//#include "SVTK_MainWindow.h"
#include "SVTK_NonIsometricDlg.h"
#include "SVTK_CubeAxesDlg.h"
#include "SVTK_FontWidget.h"
#include "SVTK_ViewModel.h"
#include "SVTK_ViewWindow.h"
#include "SVTK_Renderer.h"
#include "SVTK_InteractorStyle.h"
#include "SVTK_RenderWindowInteractor.h"
#include "SVTK_GenericRenderWindowInteractor.h"
#include "SVTK_Selector.h"
#include "SVTK_Selection.h"
#include "SVTK_SelectionEvent.h"
#include "SVTK_SpaceMouse.h"
#include "SVTK_Event.h"
#include "SVTK_ViewModelBase.h"

int
main(int argc, char** argv)
{
  return 0;
}
