# Copyright (C) 2007-2012  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

#  File   : Makefile.in
#  Author : Nicolas REJNERI
#  Module : SALOME
#
include $(top_srcdir)/adm_local/unix/make_common_starter.am

# extra source files (generated by sip)
SIP_SRC = sipAPISalomePyQt.h			\
	  sipSalomePyQtSalomePyQt.cc		\
	  sipSalomePyQtSALOME_Selection.cc	\
	  sipSalomePyQtQtxAction.cc		\
	  sipSalomePyQtQtxActionGroup.cc	\
	  sipSalomePyQtQtxActionSet.cc		\
	  sipSalomePyQtcmodule.cc

# Sip definition file
SIP_FILES = SalomePyQt.sip

# extra dist files
EXTRA_DIST += $(SIP_FILES)

# extra clean files
CLEANFILES = $(SIP_SRC)

# moc files (generated my moc)
MOC_FILES = SalomePyQt_moc.cxx

# compilation flags
COMMON_CPP_FLAGS = $(QT_INCLUDES) $(QWT_INCLUDES) $(SIP_INCLUDES) $(PYTHON_INCLUDES)	 \
	$(CAS_CPPFLAGS) $(VTK_INCLUDES) $(OGL_INCLUDES) $(BOOST_CPPFLAGS)		 \
	-DHAVE_CONFIG_H @KERNEL_CXXFLAGS@ -I$(top_builddir)/salomeadn/unix		 \
	-I$(srcdir) -I$(top_builddir)/idl						 \
	-I$(srcdir)/../../SUIT -I$(srcdir)/../../CAM -I$(srcdir)/../../STD		 \
	-I$(srcdir)/../../Qtx -I$(srcdir)/../../Event	                                 \
	-I$(srcdir)/../../LogWindow			                                 \
	-I$(srcdir)/../../VTKViewer                     				 \
	-I$(srcdir)/../../OCCViewer -I$(srcdir)/../../Plot2d				 \
	-I$(srcdir)/../SALOME_PYQT_GUILight -I$(srcdir)/../../PyInterp			 \
	-I$(srcdir)/../../LightApp -I$(srcdir)/../../ObjBrowser				 \
	-I$(srcdir)/../../OBJECT							

# linkage flags
COMMON_LIBS = $(PYTHON_LIBS) $(SIP_LIBS) $(PYQT_LIBS) $(VTK_LIBS) $(QWT_LIBS)		 \
	$(OGL_LIBS) ../../SUIT/libsuit.la ../../CAM/libCAM.la ../../STD/libstd.la	 \
	../../Qtx/libqtx.la ../../Event/libEvent.la	                                 \
	../../LogWindow/libLogWindow.la		                                         \
	../../VTKViewer/libVTKViewer.la                         			 \
	../SALOME_PYQT_GUILight/libSalomePyQtGUILight.la ../../OCCViewer/libOCCViewer.la \
	../../Plot2d/libPlot2d.la

# libraries targets
lib_LTLIBRARIES = libSalomePyQt.la

# library sources / rules
dist_libSalomePyQt_la_SOURCES = SalomePyQt.cxx
nodist_libSalomePyQt_la_SOURCES = $(MOC_FILES) $(SIP_SRC)
libSalomePyQt_la_CPPFLAGS = $(COMMON_CPP_FLAGS)
libSalomePyQt_la_LIBADD = $(COMMON_LIBS)

# custom build step: generate C++ wrapping according to $(SIP_FILES)
$(SIP_SRC): $(SIP_FILES)
	$(SIP) $(PYQT_SIPFLAGS) $<

# custom install step: create symbolic link
install-exec-hook: 
	(cd $(DESTDIR)$(libdir); ln -sf libSalomePyQt.so SalomePyQt.so)

# custom uninstall step: remove symbolic link
uninstall-hook: 
	rm -f $(DESTDIR)$(libdir)/SalomePyQt.so
